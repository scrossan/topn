#!/usr/bin/env python3
import argparse
import heapq
import logging
import os
import sys
import time

logging.basicConfig(level=logging.INFO)


def main(args):
    data_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), args.data)
    if not os.path.exists(data_path):
        logging.error("Path {} does not exist! Ensure --data exists and "
                      "is the relative path to the file.".format(data_path))
        sys.exit(1)

    nums = []
    heapq.heapify(nums)
    start = time.process_time()
    with open(data_path) as f:
        for line in f:
            # Make sure the numbers are ints, otherwise they
            # are not ordered properly when popping them
            num = int(line.strip())

            # If the heap has fewer than topn elements add the num
            if len(nums) < args.topn:
                heapq.heappush(nums, num)
                logging.debug("Pushing {} to heap".format(num))

            # If num is greater than the smallest num in the heap
            # add num and remove the smallest num
            elif num > nums[0]:
                smallest_num = heapq.heappushpop(nums, num)
                logging.debug("Popped {}, pushed {} to heap".format(smallest_num, num))

    logging.info("Took {}s to process data".format(time.process_time() - start))
    logging.info("The top {} numbers are: {}".format(args.topn, heapq.nlargest(args.topn, nums)))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Outputs the largest N numbers in a given file,"
                                     " assuming that the file contains a single number on each line.")
    parser.add_argument("-n", "--topn", default=5, type=int,
                        help="the top n numbers to find in the data (default: 5)")
    parser.add_argument("-d", "--data", default="data.txt",
                        help="relative path to the data to process (default: data.txt)")
    args = parser.parse_args()
    main(args)
