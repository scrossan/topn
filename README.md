# topN

Requires Python 3. Written using 3.7.4.

Generate some data sets:
```bash
for num in {1..1000000}; do echo $num; done | shuf > data-1mil.txt
for num in {1..10000000}; do echo $num; done | shuf > data-10mil.txt
for num in {1..100000000}; do echo $num; done | shuf > data-100mil.txt
```

Find the top n numbers in a set:
```bash
python3 topn.py --data data-1mil.txt
python3 topn.py --data data-10mil.txt --topn 10
```

Note: as each set contains the numbers 1 to 1, 10 or 100 million shuffled, the top n numbers will always be the n numbers before 1, 10 or 100 million (inclusive).
